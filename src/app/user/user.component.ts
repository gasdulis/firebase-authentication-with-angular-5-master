import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/user.service';
import { AuthService } from '../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseUserModel } from '../core/user.model';
import {HttpClient} from "@angular/common/http";

interface Subjects {
  code:string;
  data: {
    name: string,
    semester: number
  }
}

@Component({
  selector: 'page-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.scss']
})
export class UserComponent implements OnInit{

  //apiRoot = 'https://totapp-paualos3.c9users.io';  //Develop
  apiRoot = 'https://totapp-isa.herokuapp.com';  //Production
  subjectsUrl = `${this.apiRoot}/api/subjects`;

  user: FirebaseUserModel = new FirebaseUserModel();
  profileForm: FormGroup;
  subjects: Subjects[];

  constructor(
    public userService: UserService,
    public authService: AuthService,
    private route: ActivatedRoute,
    private location : Location,
    private router: Router,
    private fb: FormBuilder,
    private http:HttpClient
  ) {

  }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.user = data;
        console.log("user", this.user)
        this.createForm(this.user.name);
      }
    })
    this.fetchSubjects();
  }

  fetchSubjects() {
    this.http.get(this.subjectsUrl).subscribe((data:any[]) => {
      this.subjects = data;
    })
  };

  createForm(name) {
    this.profileForm = this.fb.group({
      name: [name, Validators.required ]
    });
  }

  save(value){
    this.userService.updateCurrentUser(value)
    .then(res => {
      console.log(res);
    }, err => console.log(err))
  }

  logout(){
    this.authService.doLogout()
    .then((res) => {
      this.router.navigate(['/login']);    }, (error) => {
      console.log("Logout error", error);
    });
  }
}
