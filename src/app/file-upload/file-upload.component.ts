import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { finalize } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { UserService } from '../core/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import { FirebaseUserModel } from '../core/user.model';
import {AuthService} from '../core/auth.service';
import {Location} from '@angular/common';
import { FormsModule } from '@angular/forms';

interface Subjects {
  code:string;
  data: {
    name: string,
    semester: number
  }
}

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {


  //apiRoot = 'https://totapp-paualos3.c9users.io' //Develop
  apiRoot = 'https://totapp-isa.herokuapp.com'   //Production
  filesUrl = `${this.apiRoot}/api/files`
  subjectsUrl = `${this.apiRoot}/api/subjects`
  subjects: Subjects[]
  selectedSubject: string
  fileName: string
  description: string
  fileDownloadUrl = ""
  user: FirebaseUserModel = new FirebaseUserModel()

  // Main task
  task: AngularFireUploadTask;

  // Progress monitoring
  percentage: Observable<number>

  snapshot: Observable<any>

  // Download URL
  downloadURL: Observable<string>

  // State for dropzone CSS toggling
  isHovering: boolean;

  constructor(private storage: AngularFireStorage,
              private db: AngularFirestore,
              private route: ActivatedRoute,
              private router: Router,
              private http:HttpClient,
              private location : Location,
              public userService: UserService,
              public authService: AuthService) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.user = data;
        console.log("user", this.user)
      }
    })
    this.fetchSubjects();
  }

  fetchSubjects() {
    this.http.get(this.subjectsUrl).subscribe((data:any[]) => {
      this.subjects = data;
      this.selectedSubject = data[0].code
    })
  };

  toggleHover(event: boolean) {
    this.isHovering = event;
  }


  startUpload(event: FileList) {
    // The File object
    const file = event.item(0)
    //this.fileName = file.name
    // Client-side validation example
    /*if (file.type.split('/')[0] !== 'pdf') {
      console.error('unsupported file type :( ')
      return;
    }*/

    // The storage path
    const path = `totapp/${new Date().getTime()}_${file.name}`;

    // Totally optional metadata
    const customMetadata = { app: 'TOTAPP!' };

    // The main task
    this.task = this.storage.upload(path, file, { customMetadata })

    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(snap => {
        console.log(snap)
        if (snap.bytesTransferred === snap.totalBytes) {
          // Update firestore on completion
          this.db.collection('photos').add( { path, size: snap.totalBytes })
        }
      })
    )


    // The file's download URL
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL()
      this.downloadURL.subscribe(url => {
        this.fileDownloadUrl = url
        console.log("download URL", this.fileDownloadUrl)
        this.postFileUploaded();
      })
    })).subscribe();
  }

  postFileUploaded() {
    console.log("doing post", {
      author: this.user.name,
      subject: this.selectedSubject,
      url: this.fileDownloadUrl,
      name: this.fileName,
      description: this.description
    })
    this.http.post(this.filesUrl, {
      author: this.user.name,
      subject: this.selectedSubject,
      url: this.fileDownloadUrl,
      name: this.fileName,
      description: this.description
    }).subscribe((data: any[]) => {
      console.log("post result", data)
      location.reload()
    })
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }

  logout(){
    this.authService.doLogout()
      .then((res) => {
        this.router.navigate(['/login']);
      }, (error) => {
        console.log("Logout error", error);
      });
  }
}
