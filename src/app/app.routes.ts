import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './register/register.component';
import { UserResolver } from './user/user.resolver';
import { SubjectComponent } from './subject/subject.component';
import { AuthGuard } from './core/auth.guard';
import { FileUploadComponent } from './file-upload/file-upload.component';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
  { path: 'subjects', component: UserComponent, resolve: { data: UserResolver} },
  { path: 'subjects/:id', component: SubjectComponent, resolve: { data: UserResolver} },
  { path: 'files', component: SubjectComponent, resolve: { data: UserResolver} },
  { path: 'upload', component: FileUploadComponent, resolve: { data: UserResolver} }
];
